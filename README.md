# Redux Integration 

This application provides an overview of how Redux is integrated into a React application. It is a bird watching test application in which users will be able to add birds they have seen and increment a bird each time they see it again.

## What is Redux

Redux is a popular data store for JavaScript and React applications. It follows a central principle that data binding should flow in one direction and should be stored as a single source of truth. Redux gained popularity because of the simplicity of the design concept and the relatively small implementation.

Redux operates according to a few concepts. First, the store is a single object with fields for each selection of data. You update the data by dispatching an action that says how the data should change. You then interpret actions and update the data using reducers. Reducers are functions that apply actions to data and return a new state, instead of mutating the previous state.

## Getting Started

To get started, clone the repository and navigate to the project directory. Then, install the dependencies using the following command:

```
npm install
```

Once the dependencies are installed, you can start the application using the following command:

```
npm start
```

The application will then be available at http://localhost:3000/.

## Installation

To use Redux in your React application, you need to install the following packages:

- `react-redux`: This package provides the bindings for React and Redux.

- `redux`: This is the Redux library itself.

You can install these packages using the following command:

```
npm install react-redux redux
```

## File Structure

We have the following file structure:

- `App.js`: This is the main component where Redux is integrated.

- `bird.js`: This file contains `Redux actions`, `reducers`, and `selectors` related to bird data.

- `style.css`: A CSS file for styling the component.

- `index.js`: This file integrates `redux` for state management

- `store.js`: This file holds the whole state tree of your application. 

![fileStructure](./src/img/Screenshot%20from%202023-05-19%2014-51-34.png)

## Redux Integration Steps


### 1. Setting Up a Store

First, you need to connect Redux to your project. To use Redux, you’ll need to wrap your root components with a Provider to ensure that the store is available to all child components in the tree. To do that, import the Provider component from the react-redux package in your `app.js` file. Add the Provider to your root component around any other components by making the following highlighted changes to your code:

```
import React from 'react';
import { Provider } from 'react-redux'
import { createStore } from 'redux'

const Application=()=>{
return(
      <Provider>
        <App />
      </Provider>
  );
  
}
export default Application

```

Now that you have wrapped your components, it’s time to add a store. To do that, create a file `store.js`. Import the createStore function from redux, then pass a function that returns an object. The `store.js` in this application contains a function `configureStore` that configures and creates a redux store. This function is used for setting up the initial state and any middleware for the store.

```
import { createStore, applyMiddleware } from "redux";

export default function configureStore(initialState) {
    return createStore();
}
```

### 2. Creating Actions and Reducers
Next, you’ll create actions to add a bird and to increment a view. You’ll then make a reducer that will update the information depending on the action type. Finally, you’ll use the reducers to create a default store using combineReducers.

Actions are the message you send to the data store with the intended change. Reducers take those messages and update the shared store by applying the changes depending on the action type. Your components will send the actions they want your store to use, and your reducers will use actions to update the data in the store. You never call reducers directly, and there are cases where one action may impact several reducers.

Make a file called `birds.js` which will contain the actions and reducers specifically for updating your bird data.

First, you will send  action from a component to your store using a method called dispatch. An action must return an object with a type field. Otherwise, the return object can include any additional information you want to send.

Create a function called `addBirds` that takes a bird as an argument and returns an object containing a type of `ADD_BIRD` and the bird as a field:

```
export function addBird(bird) {
  return {
    type: 'ADD_BIRD',
    bird,
  }
}

```

Create a const called `ADD_BIRD` that saves the string 'ADD_BIRD'. Then update the action:

```
const ADD_BIRD = 'ADD_BIRD';

export function addBird(bird) {
  return {
    type: ADD_BIRD,
    bird,
  }
}

```

Now that you have an action, create a reducer that will respond to the action.

Reducers are functions that will determine how a state should change based on actions. The actions don’t make changes themselves, the reducers will take the state and make changes based on actions.

A reducer receives two arguments: the current state and the action. The current state refers to the state for a particular section of the store. Inside birds.js create a reducer called `birds` that takes state and action and returns the state without any changes:

```
const ADD_BIRD = 'ADD_BIRD';

export function addBird(bird) {
  return {
    type: ADD_BIRD,
    bird,
  }
}

function birds(state, action) {
  return state;
}

```

You will not use the reducer directly and instead will combine them into a usable collection that you will export and use to create your base store in index.js. Notice also that you need to return the state if there are no changes. Redux will run all the reducers anytime you dispatch an action, so if you don’t return state you risk losing your changes.

Finally, since Redux returns the state if there are no changes, add a default state using default parameters.

Create a defaultBirds array that will have the placeholder bird information. Then update the state to include defaultBirds as the default parameter:

```
const ADD_BIRD = 'ADD_BIRD';

export function addBird(bird) {
  return {
    type: ADD_BIRD,
    bird,
  }
}

const defaultBirds = [
  {
    name: 'robin',
    views: 1,
  }
];

function birds(state=defaultBirds, action) {
  return state;
}
```
Now that you have a reducer returning your state, you can use the action to apply the changes. The most common pattern is to use a `switch` on the `action.type` to apply changes.

Create a switch statement that will look at the action.type. If the case is ADD_BIRD, spread out the current state into a new array and add the bird with a single view:

```
const ADD_BIRD = 'ADD_BIRD';

export function addBird(bird) {
  return {
    type: ADD_BIRD,
    bird,
  }
}

const defaultBirds = [
  {
    name: 'robin',
    views: 1,
  }
];

function birds(state=defaultBirds, action) {
  switch (action.type) {
    case ADD_BIRD:
      return [
        ...state,
        {
          name: action.bird,
          views: 1
        }
      ];
    default:
      return state;
  }
}
```

Now that you have one action, you can create an action for incrementing a view.

Create an action called `incrementBird`. Like the addBird action, this will take a bird as an argument and return an object with a type and a bird. The only difference is the type will be `INCREMENT_BIRD`:

```
const ADD_BIRD = 'ADD_BIRD';
const INCREMENT_BIRD = 'INCREMENT_BIRD';

export function addBird(bird) {
  return {
    type: ADD_BIRD,
    bird,
  }
}

export function incrementBird(bird) {
  return {
    type: INCREMENT_BIRD,
    bird
  }
}

const defaultBirds = [
  {
    name: 'robin',
    views: 1,
  }
];

function birds(state=defaultBirds, action) {
  switch (action.type) {
    case ADD_BIRD:
      return [
        ...state,
        {
          name: action.bird,
          views: 1
        }
      ];
    default:
      return state;
  }
}
```

Inside of birds add a new case for `INCREMENT_BIRD`. Then pull the bird you need to increment out of the array using `find()` to compare each name with the action.bird. You have the bird you need to change, but you need to return a new state containing all the unchanged birds as well as the bird you’re updating. Select all remaining birds with `state.filter` by selecting all birds with a name that does not equal action.name. Then return a new array by spreading the birds array and adding the bird at the end. Finally, update the bird by creating a new object with an incremented view:

```
const ADD_BIRD = 'ADD_BIRD';
...
function birds(state=defaultBirds, action) {
  switch (action.type) {
    case ADD_BIRD:
      return [
        ...state,
        {
          name: action.bird,
          views: 1
        }
      ];
    case INCREMENT_BIRD:
      const bird = state.find(b => action.bird === b.name);
      const birds = state.filter(b => action.bird !== b.name);
      return [
        ...birds,
        {
          ...bird,
          views: bird.views + 1
        }
      ];
    default:
      return state;
  }
}
```
Now you have two complete actions and a reducer. The final step is to export the reducer so that it can initialize the store. To do this, Redux has a helper function called combineReducers that combines the reducers for you.

Inside of birds.js, import combineReducers from redux. Then call the function with birds and export the result:

```
const birdApp = combineReducers({
  birds
});

export default birdApp;

```

The final step is to initialize your store using the combined reducers.
Import the `birdApp` from `birds.js` in `App.js` Then initialize the store using birdApp:

```
import React from 'react';
import './Store/style.css';
import App from './Store/index';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import birdApp from './Store/bird';

const store = createStore(birdApp);

const Application=()=>{
return(
      <Provider store={store}>
        <App />
      </Provider>
  );
  
}
export default Application

```

### 3. Step 3 — Dispatching Changes in a Component

Now that you have working actions, you need to connect them to your events so that you can update the store. The method you will use is called dispatch and it sends a particular action to the Redux store. When Redux receives an action you have dispatched, it will pass the action to the reducers and they will update the data.

Inside of `index.js` import the Hook `useDispatch` from `react-redux`. Then call the function to create a new dispatch function. Import `incrementBird` from the `store`. Then create an onClick event on the button. When the user clicks on the button, call `incrementBird` with `bird.name` and pass the result to dispatch. To make things more readable, call the incrementBird function inside of dispatch. 

Next, you need to dispatch the addBird action. This will take two steps: saving the input to an internal state and triggering the dispatch with onSubmit.

Use the useState Hook to save the input value. Be sure to convert the input to a controlled component by setting the value on the input.

Next, import `addBird` from `birds.js`, then create a function called `handleSubmit`. Inside the handleSubmit function, prevent the page form submission with `event.preventDefault`, then dispatch the `addBird` action with the `birdName` as an argument. After dispatching the action, call `setBird('')` to clear the input. Finally, pass handleSubmit to the onSubmit event handler on the form.

To prevent an unexpected change in the components, you can sort the data in your component. Add a `sort()` function to the birds array.

```
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { addBird, incrementBird } from './bird';
import './style.css';

function App() {
  const [birdName, setBird] = useState('');
  const birds = [...useSelector(state => state.birds)].sort((a, b) => {
    return a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1;
  });
  const dispatch = useDispatch();

  const handleSubmit = event => {
    event.preventDefault();
    dispatch(addBird(birdName))
    setBird('');
  };

  return (
    <div className="wrapper">
      <h1>Bird List</h1>
      <form onSubmit={handleSubmit}>
        <label>
          <p>
            Add Bird
          </p>
          <input
            type="text"
            onChange={e => setBird(e.target.value)}
            value={birdName}
          />
        </label>
        <div>
          <button type="submit">Add</button>
        </div>
      </form>
      <ul>
        {birds.map(bird => (
          <li key={bird.name}>
            <h3>{bird.name}</h3>
            <div>
              Views: {bird.views}
              <button onClick={() => dispatch(incrementBird(bird.name))}><span role="img" aria-label="add">➕</span></button>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default App;

```

Save the file. When you do, the components will stay in alphabetical order as you increment birds.

## Conclusion

By following the steps outlined in this readme, the given code integrates Redux into a React application. Redux allows for centralized state management, making it easier to manage and update the application state across different components.
You now have a working application that combines a Redux store and your custom components. 


