import React from 'react';
import './Store/style.css';
import App from './Store/index';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import birdApp from './Store/bird';

const store = createStore(birdApp);

const Application=()=>{
return(
      <Provider store={store}>
        <App />
      </Provider>
  );
  
}
export default Application
